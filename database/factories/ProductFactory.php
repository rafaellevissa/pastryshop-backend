<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->text(10),
            'price' => fake()->randomNumber(3),
            'picture' => fake()->randomElement(['default/beverage.jpg', 'default/food.jpg']),
            'description' => fake()->text(255),
            'flavor' => fake()->text(10)
        ];
    }
}
