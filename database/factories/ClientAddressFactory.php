<?php

namespace Database\Factories;

use App\Models\ClientAddress;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientAddressFactory extends Factory
{
    protected $model = ClientAddress::class;

    public function definition()
    {
        return [
            'street' => fake()->address(),
            'complement' => fake()->address(),
            'neighborhood' => fake()->address(),
            'zipcode' => fake()->randomNumber(),
            'client_id' => null
        ];
    }
}
