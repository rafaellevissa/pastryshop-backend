<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{
  public function products(): array
  {
    return Product::orderBy('created_at', 'DESC')->get()->toArray();
  }

  public function product(int $id): array
  {
    return Product::findOrFail($id)->toArray();
  }

  public function store(array $payload): array
  {
    return Product::create($payload)->toArray();
  }

  public function delete(int $id): bool
  {
    return Product::destroy($id);
  }

  public function update(int $id, array $payload): bool
  {
    return Product::findOrFail($id)->updateOrFail($payload);
  }
}
