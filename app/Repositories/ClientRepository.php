<?php

namespace App\Repositories;

use Exception;
use App\Models\Client;
use App\Models\ClientAddress;
use Illuminate\Support\Facades\DB;

class ClientRepository
{
  public function clients(): array
  {
    return Client::all()->toArray();
  }

  public function client(int $id): array
  {
    return Client::findOrFail($id)->toArray();
  }

  public function store(array $payload): array
  {
    DB::beginTransaction();

    try {
      $client = Client::create($payload)
        ->address()
        ->save(new ClientAddress($payload['address']))
        ->toArray();

      DB::commit();

      return $client;
    } catch (Exception $error) {
      DB::rollBack();

      throw $error;
    }
  }

  public function delete(int $id): bool
  {
    return Client::destroy($id);
  }

  public function update(int $id, array $payload): array
  {
    try {
      DB::beginTransaction();

      $client = Client::findOrFail($id);
      $client->updateOrFail($payload);
      
      $client->address()->first()->update($payload['address']);

      DB::commit();

      return $client->toArray();
    } catch (Exception $error) {
      DB::rollBack();

      throw $error;
    }
  }
}
