<?php

namespace App\Repositories;

use Exception;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class OrderRepository
{
  public function orders(): array
  {
    return Order::with('products')->get()->toArray();
  }

  public function order(int $id): array
  {
    return Order::with('products')->findOrFail($id)->toArray();
  }

  public function store(int $clientId, array $payload): array
  {
    try {
      DB::beginTransaction();

      $order = Order::create(['client_id' => $clientId]);
      $order->products()->attach($payload['products']);

      DB::commit();

      return $this->order($order->id);
    } catch (Exception $error) {
      DB::rollBack();

      throw $error;
    }
  }

  public function delete(int $id): bool
  {
    return Order::destroy($id);
  }

  public function update(int $id, array $payload): array
  {
    return Order::findOrFail($id)->products()->sync($payload['products']  );
  }
}
