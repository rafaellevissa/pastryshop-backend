<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ClientAddress extends Model
{
    use HasFactory;

    protected $fillable = ['street', 'complement', 'neighborhood', 'zipcode'];

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }
}
