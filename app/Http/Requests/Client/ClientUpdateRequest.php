<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class ClientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $userId = request()->route()->parameter('client');

        return [
            'name' => ['min:2', 'max:100', 'required'],
            'email' => ['email', 'unique:clients,email,'.$userId, 'max:255', 'required'],
            'password' => ['min:6', 'max:255'],
            'phone' => ['max:50', 'required'],
            'birthdate' => ['date', 'required'],
            'address.street' => ['max:200', 'required'],
            'address.complement' => ['max:100', 'required'],
            'address.neighborhood' => ['max:100', 'required'],
            'address.zipcode' => ['max:30', 'required']
        ];
    }
}
