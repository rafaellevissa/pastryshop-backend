<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => ['min:3', 'max:100', 'required'],
            'price' => ['numeric', 'required'],
            'picture' => ['image', 'required'],
            'flavor' => ['max:30'],
            'description' => ['max:255']
        ];
    }
}
