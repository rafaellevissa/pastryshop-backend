<?php

namespace App\Http\Controllers;

use Exception;
use App\Http\Requests\Orders\OrderStoreRequest;
use App\Http\Requests\Orders\OrderUpdateRequest;
use App\Mail\OrderMail;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @OA\Get(
     *     path="/api/orders",
     *     tags={"orders"},
     *     security={ {"bearer": {} }},
     *     @OA\Response(
     *         response="200",
     *         description="Success"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function index()
    {
        try {
            $orders = $this->orderRepository->orders();

            return response($orders);
        } catch (Exception $error) {
            return response([
                'error' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/orders/{id}",
     *     tags={"orders"},
     *     security={ {"bearer": {} }},
     *     @OA\RequestBody(
     *          required=true,
     *          description="create order",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Created"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function store(OrderStoreRequest $request)
    {
        $payload = $request->validated();
        
        try {
            $clientId = auth()->user()->id;
            $order = $this->orderRepository->store($clientId, $payload);

            Mail::to($request->user())->send(new OrderMail($order));

            return response($order, 201);
        } catch (Exception $error) {
            return response([
                'error' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/orders/{id}",
     *     tags={"orders"},
     *     security={ {"bearer": {} }},
     *     @OA\Response(
     *         response="200",
     *         description="Success"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function show(int $id)
    {
        try {
            $order = $this->orderRepository->order($id);

            return response($order);
        } catch (Exception $error) {
            return response([
                'error' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/orders/{id}",
     *     tags={"orders"},
     *     security={ {"bearer": {} }},
     *     @OA\RequestBody(
     *          required=true,
     *          description="update order",
     *     ),
     *     @OA\Response(
     *         response="201",
     *         description="Created"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function update(OrderUpdateRequest $request, int $id)
    {
        $payload = $request->validated();

        try {
            $order = $this->orderRepository->update($id, $payload);

            return response($order, 201);
        } catch (Exception $error) {
            return response([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/orders/{id}",
     *     tags={"orders"},
     *     security={ {"bearer": {} }},
     *     @OA\Response(
     *         response="201",
     *         description="Created"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function destroy(int $id)
    {
        try {
            $order = $this->orderRepository->delete($id);

            if (!$order) {
                throw new Exception('The order could not be deleted!');
            }

            return response(['message' => 'Order deleted successfully'], 201);
        } catch (Exception $error) {
            return response([
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
