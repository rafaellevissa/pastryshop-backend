<?php

namespace App\Http\Controllers;

use App\Http\Requests\Client\ClientUpdateRequest;
use App\Repositories\ClientRepository;
use Exception;

class ClientController extends Controller
{
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @OA\Get(
     *     path="/api/clients",
     *     tags={"clients"},
     *     security={ {"bearer": {} }},
     *     @OA\RequestBody(
     *          required=true,
     *          description="Client's list",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Success"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function index()
    {
        try {
            $clients = $this->clientRepository->clients();

            return response($clients);
        } catch (Exception $error) {
            return response([
                'error' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/clients/{id}",
     *     tags={"clients"},
     *     security={ {"bearer": {} }},
     *     @OA\RequestBody(
     *          required=true,
     *          description="Get client by its id",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Success"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function show(int $id)
    {
        try {
            $client = $this->clientRepository->client($id);

            return response($client);
        } catch (Exception $error) {
            return response([
                'error' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/clients/{id}",
     *     tags={"clients"},
     *     security={ {"bearer": {} }},
     *     @OA\RequestBody(
     *          required=true,
     *          description="update client by its id",
     *     ),
     *     @OA\Response(
     *         response="201",
     *         description="Created"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function update(ClientUpdateRequest $request, int $id)
    {
        $payload = $request->validated();

        try {
            $client = $this->clientRepository->update($id, $payload);

            return response($client, 201);
        } catch (Exception $error) {
            return response([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/clients/{id}",
     *     tags={"clients"},
     *     security={ {"bearer": {} }},
     *     @OA\RequestBody(
     *          required=true,
     *          description="delte client by its id",
     *     ),
     *     @OA\Response(
     *         response="201",
     *         description="Created"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function destroy(int $id)
    {
        try {
            $client = $this->clientRepository->delete($id);

            if (!$client) {
                throw new Exception('The client could not be deleted!');
            }

            return response(['message' => 'client deleted successfully'], 201);
        } catch (Exception $error) {
            return response([
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
