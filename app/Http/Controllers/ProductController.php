<?php

namespace App\Http\Controllers;

use Exception;
use App\Http\Requests\Products\ProductStoreRequest;
use App\Http\Requests\Products\ProductUpdateRequest;
use App\Repositories\ProductRepository;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class ProductController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @OA\Get(
     *     path="/api/products",
     *     tags={"products"},
     *     security={ {"bearer": {} }},
     *     @OA\Response(
     *         response="200",
     *         description="Success"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function index()
    {
        try {
            $products = $this->productRepository->products();

            return response($products);
        } catch (Exception $error) {
            return response([
                'error' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/products/{id}",
     *     tags={"products"},
     *     security={ {"bearer": {} }},
     *     @OA\RequestBody(
     *          required=true,
     *          description="create product",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Created"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function store(ProductStoreRequest $request)
    {
        $payload = $request->validated();

        try {
            $path = $request->picture->storeAs(
                $request->user()->id . '/products',
                Uuid::uuid4() . '.png',
                's3'
            );

            $payload['picture'] = $path;

            $product = $this->productRepository->store($payload);

            return response($product, 201);
        } catch (Exception $error) {
            return response([
                'error' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/products/{id}",
     *     tags={"products"},
     *     security={ {"bearer": {} }},
     *     @OA\Response(
     *         response="200",
     *         description="Success"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function show(int $id)
    {
        try {
            $product = $this->productRepository->product($id);

            return response($product);
        } catch (Exception $error) {
            return response([
                'error' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/products/{id}",
     *     tags={"products"},
     *     security={ {"bearer": {} }},
     *     @OA\RequestBody(
     *          required=true,
     *          description="update products",
     *     ),
     *     @OA\Response(
     *         response="201",
     *         description="Created"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function update(ProductUpdateRequest $request, int $id)
    {
        $payload = $request->validated();

        try {
            if ($request->hasFile('picture')) {
                $product = $this->productRepository->product($id);

                Storage::disk('s3')->delete($product['picture']);

                $path = $request->picture->storeAs(
                    $request->user()->id . '/products',
                    Uuid::uuid4() . '.png',
                    's3'
                );

                $payload['picture'] = $path;
            }

            $product = $this->productRepository->update($id, $payload);

            return response($product, 201);
        } catch (Exception $error) {
            return response([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/products/{id}",
     *     tags={"products"},
     *     security={ {"bearer": {} }},
     *     @OA\Response(
     *         response="201",
     *         description="Created"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function destroy(int $id)
    {
        try {
            $product = $this->productRepository->delete($id);

            if (!$product) {
                throw new Exception('The product could not be deleted!');
            }

            return response(['message' => 'Product deleted successfully'], 201);
        } catch (Exception $error) {
            return response([
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
