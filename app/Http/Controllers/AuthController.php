<?php

namespace App\Http\Controllers;

use Exception;
use App\Repositories\ClientRepository;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\SignUpRequest;

class AuthController extends Controller
{
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @OA\Post(
     *     path="/api/auth/login",
     *     tags={"auth"},
     *     @OA\RequestBody(
     *          required=true,
     *          description="Login payload",
     *          @OA\JsonContent(
     *              required={"email", "password"},
     *              @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *              @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Success",
     *         @OA\JsonContent(
     *              @OA\Property(property="access_token", type="string", format="string", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwMDAvYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE2NTkyMDc3NzAsImV4cC"),
     *              @OA\Property(property="token_type", type="string", format="string", example="bearer"),
     *              @OA\Property(property="expires_in", type="number", format="integer", example=3600),
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function login(LoginRequest $request)
    {
        $payload = $request->validated();

        try {
            if (!$token = auth()->attempt($payload)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }

            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60
            ]);
        } catch (Exception $error) {
            return response([
                'error' => $error->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/auth/signup",
     *     tags={"auth"},
     *     @OA\RequestBody(
     *          required=true,
     *          description="Signup payload",
     *     ),
     *     @OA\Response(
     *         response="201",
     *         description="Created"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized"
     *     ),
     *      @OA\Response(
     *         response="400",
     *         description="Bad Request"
     *     ),
     *      @OA\Response(
     *         response="500",
     *         description="Unexpected"
     *     )
     * )
     */
    public function signup(SignUpRequest $request)
    {
        $payload = $request->validated();

        try {
            $client = $this->clientRepository->store($payload);

            return response($client, 201);
        } catch (Exception $error) {
            return response([
                'error' => $error->getMessage()
            ], 500);
        }
    }
}
