<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Client;
use App\Models\ClientAddress;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

use function PHPUnit\Framework\assertSame;

class ClientTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    public function test_client_list()
    {
        $clients = Client::factory(10)->create()->toArray();

        $response = $this->get('/api/clients');

        $response->assertStatus(200);
        $response->assertJson($clients);
    }

    public function test_find_client()
    {
        $client = Client::factory()->create()->toArray();

        $response = $this->get('/api/clients/' . $client['id']);

        $response->assertOk();
        $response->assertJson($client);
    }

    public function test_update_client()
    {
        $client = Client::factory()->create(['name' => 'sldk']);
        
        $client->each(function ($client) {
            $client->address()->save(ClientAddress::factory()->create(['client_id' => $client->id]));
        });

        $payload = [
            'name' => 'levi',
            'email' => 'levi@email.com',
            'password' => 'test123',
            'birthdate' => '1997-01-01',
            'phone' => '12121212',
            'address' => [
                'street' => 'Harlem street',
                'complement' => 'Second block',
                'neighborhood' => 'neighborhood',
                'zipcode' => '127347'
            ]
        ];

        $response = $this->put('/api/clients/' . $client->id, $payload);

        $response->assertCreated();
        $responsePayload = $response->decodeResponseJson();
        assertSame($responsePayload['name'], $payload['name']);
    }

    public function test_delete_client()
    {
        $client = Client::factory()->create();

        $response = $this->delete('/api/clients/' . $client->id);

        $response->assertCreated();
    }
}
