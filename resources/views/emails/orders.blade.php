<h1>Thank you for your order!</h1>

<p>Order: {{ $order['id'] }}</p>

<h2>Products</h2>
<table>
  <thead>
    <tr>
      <td>ID</td>
      <td>Name</td>
      <td>Price</td>
    </tr>
  </thead>
  <tbody>
    @foreach ($order['products'] as $product)
    <tr>
      <td>{{ $product['id'] }}</td>
      <td>{{ $product['name'] }}</td>
      <td>{{ $product['price'] }}</td>
    </tr>
    @endforeach
  </tbody>
</table>