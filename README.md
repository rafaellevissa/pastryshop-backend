# PASTRYSHOP BACKEND

This is a rest API made with `laravel`, the technologies used can be seen down bellow:

| NAME       | VERSION |
| ---------- | ------- |
| laravel    | 9.19    |
| php        | 8.0.2   |

### DOCKER USAGE

It was created a `Dockerfile` so then the project can be up and running easily typing the folowing command, do not forget to create the `.env` file:

```
docker build -t pastryshop-backend .
```

```
docker run -p 8000:80 --name pastry-backend pastry-backend 
```

After that, you can look it running at `http://localhost:8000`.

### LOCALLY USAGE

You might prefer run it locally, to do so, make sure the environment is set up propertly with all the requirements needed to run a laravel application. Do not forget to create the `.env` file.

Install the dependencies as shown below:

```
composer install
```

Once everything installed, you will need to run:

```
php artisan serve
```

Everything should be up and running at `http://localhost:8000`.

#### USER CREDENTIALS:

```
email: client@email.com
password: password
```

### TESTS

It's been using `php unit` to run the tests, so make sure you install all the dependencies with `composer install` before running the tests:

```
php artisan test
```
